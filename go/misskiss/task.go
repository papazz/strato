package misskiss

import (
	"fmt"
	"time"
)

func f(iter chan int, fback chan string) {
	for {
		i := <-iter
		select {
		case <-time.NewTicker(50 * time.Millisecond).C:
			switch {
			case i%3 == 0 && i%5 == 0:
				fback <- fmt.Sprintf("%s, %d", "MissKiss", i)
			case i%3 == 0:
				fback <- fmt.Sprintf("%s, %d", "Miss", i)
			case i%5 == 0:
				fback <- fmt.Sprintf("%s, %d", "Kiss", i)
			default:
				fback <- fmt.Sprintf("%d", i)
			}
		}
	}
}
