package misskiss

import (
	"fmt"
	"testing"
)

func TestF(t *testing.T) {
	iter := make(chan int)
	feed := make(chan string)
	go f(iter, feed)
	for i := 1; i <= 100; i++ {
		iter <- i
		fmt.Println(<-feed)
	}
}
