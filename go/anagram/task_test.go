package anagram

import (
	"fmt"
	"testing"
)

func TestMatch(t *testing.T) {
	left := []byte("AliceBob")
	right := []byte("BAliceob")
	fmt.Println(match(left, right, len(left), len(right)))
}
