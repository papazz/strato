package anagram

import (
	"bytes"
)

func isIn(s []byte, b byte) bool {
	return bytes.IndexByte(s, b) > -1
}

func dropI(right []byte, b byte) []byte {
	i := bytes.IndexByte(right, b)
	if i < 0 {
		return right
	}
	newBuf := make([]byte, len(right)-1)
	copy(newBuf, right[:i])
	copy(newBuf[i:], right[i+1:])
	return newBuf
}

func match(left, right []byte, ll, lr int) bool {
	if ll == 0 && lr == 0 {
		return true
	} else if ll == lr {
		head := left[0]
		ll--
		return isIn(right, head) && match(left[1:], dropI(right, head), ll, ll)
	}
	return false
}
