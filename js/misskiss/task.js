!function() {
    var i = 0;
    var iterator = setInterval(function() {
        i++;
        if (i > 100) {
             return clearInterval(iterator);
        }
        let msg = (i % 3 === 0) ? 'Miss' : '';
        if (i % 5 === 0) {
          msg += 'Kiss';
        } else if (msg === '') {
          msg = i;
        }
        console.log(msg);
    }, 50);
}();